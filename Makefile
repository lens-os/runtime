.PHONY: all clean

all: librt0.a

clean:
	rm librt0.a rt0.o

librt0.a: rt0.o
	ar r librt0.a rt0.o

rt0.o: rt0.asm
	nasm rt0.asm -f elf64 -o rt0.o
