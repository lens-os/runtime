use std::process::Command;

fn main() {
    println!("cargo:rustc-link-search=.");
    Command::new("make").output().expect("Make failed"); // Eat output.
}
